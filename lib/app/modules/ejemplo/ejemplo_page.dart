import 'package:app_ejemplo/app/modules/ejemplo/ejemplo_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class EjemploPage extends StatelessWidget {
  const EjemploPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<EjemploController>(
      builder: (ctrl) => Scaffold(
        body: Center(
          child: Text(ctrl.getTexto()),
        ),
      ),
    );
  }
}
