import 'package:app_ejemplo/app/modules/ejemplo/ejemplo_controller.dart';
import 'package:get/get.dart';

class EjemploBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => EjemploController());
  }
}
