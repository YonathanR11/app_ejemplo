import 'package:app_ejemplo/app/data/model/post_model.dart';
import 'package:app_ejemplo/app/data/provider/post_api.dart';
import 'package:get/get.dart';

class PostRepository {
  final PostApi _api = Get.find<PostApi>();

  Future<PostModel> getPost(int id) => _api.getPost(id);
}
