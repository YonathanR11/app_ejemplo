import 'package:app_ejemplo/app/modules/splash/splash_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class SplashPage extends StatelessWidget {
  const SplashPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashController>(
      builder: (ctrl) => Container(
        color: Colors.white,
        child: Column(
          children: [
            FlutterLogo(
              size: MediaQuery.of(context).size.height - 200,
              style: FlutterLogoStyle.stacked,
            ),
            ElevatedButton(
              onPressed: () {
                ctrl.irHome();
              },
              child: Text("Ir a home"),
            ),
          ],
        ),
      ),
    );
  }
}
