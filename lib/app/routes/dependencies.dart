import 'package:app_ejemplo/app/data/provider/post_api.dart';
import 'package:app_ejemplo/app/data/repository/post_repository.dart';
import 'package:dio/dio.dart';
import 'package:get/get.dart';

class Dependencies {
  static const String baseURL = "https://jsonplaceholder.typicode.com";
  static void init() {
    Get.lazyPut(() => Dio(BaseOptions(baseUrl: baseURL)));
    Get.lazyPut<PostApi>(() => PostApi());
    Get.lazyPut<PostRepository>(() => PostRepository());
  }
}
