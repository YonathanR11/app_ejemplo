// import 'dart:io';

import 'package:app_ejemplo/app/routes/app_routes.dart';
import 'package:get/get.dart';

class SplashController extends GetxController {
  @override
  void onInit() {
    print("Controlador de Splash iniciado.");
    // sleep(Duration(seconds: 2));
    // print("Vamos a Home...");
    // Get.off(HomePage());
    // Get.offNamed(AppRoutes.ejemplo);
  }

  irHome(){
    print("Vamos a Home...");
    Get.offNamed(AppRoutes.post);
  }
}
