# APP_EJEMPLO

Instrucciones:

- Dentro de la carpeta ***lib/app/modules*** se genera una carpeta con el nombre del modulo ***"ejemplo"***, se agregan los archivos de *page*, *controller* y *binding*, con la estructura de codigo que tienen dentro esos archivos y en este orden:
	 - ejemplo_controller.dart
	 - ejemplo_page.dart
	 - ejemplo_binding.dart
- En el archivo ***lib/app/routes/app_routes.dart*** se agrega una constante estatica con el nombre de la ruta:
		
		`static  const  ejemplo  =  "/ejemplo";`
- En el archivo ***lib/app/routes/app_pages.dart*** se agrega un nuevo *GetPage*, junto con sus parametros requeridos:

	    `GetPage(name:  AppRoutes.ejemplo, page: () => EjemploPage(), binding:  EjemploBinding())`
- 