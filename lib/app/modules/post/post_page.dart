import 'package:app_ejemplo/app/modules/post/post_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PostPage extends StatelessWidget {
  const PostPage({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<PostController>(
      builder: (ctrl) => Scaffold(
        body: Center(
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Container(
              color: Colors.transparent,
              padding: EdgeInsets.all(20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextField(
                    // obscureText: true,
                    onChanged: ctrl.onIdChange,
                    decoration: InputDecoration(
                      // border: OutlineInputBorder(),
                      labelText: 'ID',
                    ),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      ctrl.getPost();
                    },
                    child: Text("Consultar"),
                  ),
                  Text("POST"),
                  Text("ID:"),
                  Text(ctrl.postModel.id.toString()),
                  Text("TITULO:"),
                  Text(ctrl.postModel.title),
                  Text("CONTENIDO:"),
                  Text(ctrl.postModel.body),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
