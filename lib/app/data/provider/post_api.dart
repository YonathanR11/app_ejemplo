import 'package:app_ejemplo/app/data/model/post_model.dart';
import 'package:dio/dio.dart';
import 'package:get/instance_manager.dart';

class PostApi {
  final Dio _dio = Get.find<Dio>();

  Future<PostModel> getPost(int id) async {
    final Response response = await _dio.get('/posts/$id');
    return PostModel.fromJson(response.data);
  }
}
