import 'package:app_ejemplo/app/modules/ejemplo/ejemplo_binding.dart';
import 'package:app_ejemplo/app/modules/ejemplo/ejemplo_page.dart';
import 'package:app_ejemplo/app/modules/home/home_binding.dart';
import 'package:app_ejemplo/app/modules/home/home_page.dart';
import 'package:app_ejemplo/app/modules/post/post_binding.dart';
import 'package:app_ejemplo/app/modules/post/post_page.dart';
import 'package:app_ejemplo/app/modules/splash/splash_binding.dart';
import 'package:app_ejemplo/app/modules/splash/splash_page.dart';
import 'package:app_ejemplo/app/routes/app_routes.dart';
import 'package:get/route_manager.dart';

class AppPages {
  static final List<GetPage> pages = [
    GetPage(
        name: AppRoutes.ejemplo,
        page: () => EjemploPage(),
        binding: EjemploBinding()),
    GetPage(
        name: AppRoutes.splash,
        page: () => SplashPage(),
        binding: SplashBinding()),
    GetPage(
        name: AppRoutes.home, page: 
        () => HomePage(),
        binding: HomeBinding()),
    GetPage(
        name: AppRoutes.post,
        page: () => PostPage(),
        binding: PostBinding()),
  ];
}
