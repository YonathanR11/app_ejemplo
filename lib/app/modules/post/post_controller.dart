import 'package:app_ejemplo/app/data/model/post_model.dart';
import 'package:app_ejemplo/app/data/repository/post_repository.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PostController extends GetxController {
  final PostRepository _repository = Get.find<PostRepository>();
  PostModel postModel = PostModel(userId: 0, id: 0, title: "", body: "");
  int _id = 1;

  void onIdChange(String id) {
    _id = int.parse(id);
  }

  Future<void> getPost() async {
    try {
      postModel = await _repository.getPost(_id);
      update();
    } catch (e) {
      if (e is DioError) {
        Get.snackbar(
          "Error",
          "Ocurrio un error al consultar el ID:$_id",
          icon: Icon(Icons.error_outline, color: Colors.white),
          snackPosition: SnackPosition.BOTTOM,
          backgroundColor: Colors.red[400],
        );
      }
    }
  }
}
