import 'package:app_ejemplo/app/modules/splash/splash_binding.dart';
import 'package:app_ejemplo/app/modules/splash/splash_page.dart';
import 'package:app_ejemplo/app/routes/app_pages.dart';
import 'package:app_ejemplo/app/routes/dependencies.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  Dependencies.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: true,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SplashPage(),
      initialBinding: SplashBinding(),
      getPages: AppPages.pages,
    );
  }
}
